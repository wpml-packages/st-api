<?php

use tad\FunctionMocker\FunctionMocker;

define( 'MAIN_DIR', __DIR__ . '/..' );

require_once MAIN_DIR . '/vendor/antecedent/patchwork/Patchwork.php';

include MAIN_DIR . '/vendor/autoload.php';
//require_once __DIR__ . '/mocks/OnActionMock.php';
//require_once __DIR__ . '/mocks/NonceMock.php';
//require_once __DIR__ . '/mocks/SendJSONMock.php';

FunctionMocker::init(
	[
		'whitelist' => [
			realpath( MAIN_DIR . '/classes' ),
		],
		'redefinable-internals' => [
			'function_exists',
		],
	]
);
